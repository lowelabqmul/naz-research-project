
#### Sequence around CpG sites ####

## install Biostrings
# source("https://bioconductor.org/biocLite.R")
# biocLite("Biostrings")
# library(Biostrings)

## read in allcpgs and cpg sequences files (local / cluster)
load(file = "/Users/nazrathnawaz/Google Drive/Research_Project/Robjects/allcpgs.Rdata")

cpg_sequences <- read.table("/Users/nazrathnawaz/Google Drive/Research_Project/cpg_sequences/out.txt")

row.names(cpg_sequences)=cpg_sequences[,1]

# interersect common cpgs
rn=intersect(row.names(cpg_sequences), row.names(allcpgs))

# new row with the cpg names 
allcpgs$V4 <- rownames(allcpgs)

# combine dataframes
out=cbind(allcpgs[rn,c(1,2,3,4)],cpg_sequences[rn,2])

out[1:10,]

table(out[,5])


barplot(table(out[,5]))
barplot(table(out[,5]),las=2)
l=grep("cgt$",out[,5])
plot(out[l,3])
l=grep("cgc$",out[,5])
plot(out[l,3])
l=grep("cga$",out[,5])
plot(out[l,3])
l=grep("cgg$",out[,5])
plot(out[l,3])

l1=grep("cgg$",out[,5])
l2=grep("cgc$",out[,5])
l3=grep("cga$",out[,5])
l4=grep("cgt$",out[,5])
# plot(out[l[order(out[l,3])],3])

points(out[l1[order(out[l1,3])],3],col="red")
points(out[l2[order(out[l2,3])],3],col="black")
points(out[l3[order(out[l3,3])],3],col="yellow")
points(out[l4[order(out[l4,3])],3],col="blue")

g <- out[l1[order(out[l1,3])],3]
c <- out[l2[order(out[l2,3])],3]
a <- out[l3[order(out[l3,3])],3]
t <- out[l4[order(out[l4,3])],3]

g <- g/length(g)
c <- c/length(c)
a <- a/length(a)
t <- t/length(t)

plot(g,col="red")
points(c,col="black")
points(a,col="yellow")
points(t,col="blue")

# difference between EXP1 and EXP1
out$V6 <- out$EXP2 - out$EXP1

g <- out[l1[order(out[l1,6])],6]
c <- out[l2[order(out[l2,6])],6]
a <- out[l3[order(out[l3,6])],6]
t <- out[l4[order(out[l4,6])],6]

plot(g,col="red")
points(c,col="black")
points(a,col="yellow")
points(t,col="blue")




length(which(abs(g)>0.3))/length(g)
# 0.01018766
c <- out[l2[order(out[l2,3])],3]
a <- out[l3[order(out[l3,3])],3]
t <- out[l4[order(out[l4,3])],3]
length(which(abs(g)>0.3))/length(g)
# 0.01018766
length(which(abs(c)>0.3))/length(c)
# 0.008607556
length(which(abs(a)>0.3))/length(a)
# 0.0109975
length(which(abs(t)>0.3))/length(t)
# 0.01108849



###############################################################################################

#  load(file = "/Users/nazrathnawaz/Google Drive/Research_Project/Robjects/sites.Rdata")

###############################################################################################


#### HIGH - LOW - HIGH 

sorted_allcpgs <- allcpgs[with(allcpgs, order(allcpgs$CHR, allcpgs$POS)),]

n <- (sorted_allcpgs[which(abs(sorted_allcpgs$EXP1) > 0.3),])


index <- NULL

for(x in 1:(nrow(n)-2)) {

	if (n$CHR[x]==n$CHR[x+2]) {

		if (abs(as.numeric(n$POS[x+2] - n$POS[x])) < 50) {

			if(n$EXP1[x] > n$EXP1[x+1] & n$EXP1[x+1] < n$EXP1[x+2]) {

#				if(abs(n$EXP1[x+2]-n$EXP1[x]) > 0.1) {

					index <- rbind(index, x) 

				}	
			}
		}
	}
}

n[index,]

# hlh <- n[index,] # 50 bp
# llh <- n[index,] # 500 bp & 0.1 difference


hlh
llh

t <- NULL

for (x in 1:length(index)) {

	t <- rbind(t,index[x])
	t <- rbind(t,index[x]+1)
	t <- rbind(t,index[x]+2)

}

n[t,]

# llh_df <- n[t,]
# hlh_df <- n[t,]

# add base distance -> within 50



###########################################################################################

## 										GenomicRanges									 ##

###########################################################################################


#### Genomics Ranges #####
source("https://bioconductor.org/biocLite.R")
biocLite("GenomicRanges")
library(GenomicRanges)

##
gr=GRanges(allcpgs[,1],IRanges(allcpgs[,2]-50,allcpgs[,2]+50))
reducedgr=reduce(gr)
ov=findOverlaps(reducedgr,gr)
tcpgs=table(as.matrix(ov)[,1])
max(tcpgs)
length(which(tcpgs>4))




gr = GRanges(seqnames = c("chr19"), strand = c("+"), 
             ranges = IRanges(start = c(1424613,1424620,1424642,1424656,1424658,
                                        1424662,1424668,1424675,1424679,1424686,1424716), 
                              width = 2))
gr

seqinfo(gr)
# gaps(gr)

#seqlengths(gr) = c("chr19" = 10)
genome(gr) = "hg19"

prob <- c(0.01593317,0.0117499,0.03093527,0.04483582,0.1058933,0.03585292,0.1547026,0.1600268,0.05158938,0.2143307,0.1741502)
values(gr) = DataFrame(score = prob)

reduce(gr)


# index[1,]
# allcpgs[1:10,]
# gr=GRanges(allcpgs[,1],IRanges(allcpgs[,2],allcpgs[,2]))
# gr=GRanges(allcpgs[,1],IRanges(allcpgs[,2]-500,allcpgs[,2]+500))
# reduce(gr)
# gr
# reducedgr=reduce(gr)
# width(reducedgr)
# width(reducedgr)!=1001
# length(which(width(reducedgr)!=1001))
# gr=GRanges(allcpgs[,1],IRanges(allcpgs[,2]-500,allcpgs[,2]+500))
# allcpgs[1:10,]
# gr
# reducedgr=reduce(gr)
# ov=findOverlaps(reducedgr,gr)
# ov
# as.matrix(ov)[,1]
# length(which(table(as.matrix(ov)[,1])>1))
# length(which(width(reducedgr)!=1001))
# tcpgs=table(as.matrix(ov)[,1])
# max(tcpgs)
# which(tcpgs==280)
# ov[,1]==106714
# as.matrix(ov)[,1]==106714
# as.matrix(ov)[as.matrix(ov)[,1]==106714,]
# tcpgs=table(as.matrix(ov)[,1])
# as.matrix(ov)[as.matrix(ov)[,1]==106714,2]
# allcpgs[as.matrix(ov)[as.matrix(ov)[,1]==106714,2],]
# width(reducedgr)[106714]

# gr=GRanges(allcpgs[,1],IRanges(allcpgs[,2]-250,allcpgs[,2]+250))
# reducedgr=reduce(gr)
# ov=findOverlaps(reducedgr,gr)
# tcpgs=table(as.matrix(ov)[,1])
# max(tcpgs)
# length(which(tcpgs>4))

