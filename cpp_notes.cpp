//
//  notes.cpp
//
//  Created by Nazrath 10R on 04/04/2016.
//  Copyright © 2016 Nazrath 10R. All rights reserved.
//


// Hello World
#include <iostream>

int main(int argc, const char * argv[]) {
    // insert code here...
    std::cout << "Hello, World";
    return 0;
}


// allow to use functions
#include <iostream>
#include <vector>
#include <string>
#include <fstream>

using namespace std;


//// if and else statements

int main() {

    int age = 81;
    int ageAtLastExam = 16;
    bool isNotIntoxicated = true;

    if ((age >= 1) && (age < 16)) {

        cout << "You can't drive" << endl;

    } else if (! isNotIntoxicated) {

        cout << "You can't drive" << endl;

    } else if (age >= 80 && ((age > 100) || ((age - ageAtLastExam) > 5 ))) {

        cout << "You can't drive" << endl;

    } else {

        cout << "You can drive" << endl;

    }

    return 0;
}



//// Switch statements

int main() {

    int greetingOption = 3;

    switch (greetingOption) {

        case 1:
            cout << "bonjour" << endl;
            break;

        case 2:
            cout << "Hola" << endl;
            break;

        case 3:
            cout << "Hallo" << endl;
             break;

        default:
            cout << "Hello" << endl;

    }

    return 0;

}


// Ternary operator

int main() {

    variable = (condition) ? true : false

    int largestNum = (5>2) ? 5 : 2;

    return 0;

}


//// Arrays


int main() {

    int myFavNums[5];

    int badNums[5] = {1,3,5,7,9};

    cout << "Bad number 1: " << badNums[0] << endl;

    char myName[5][5] = {{'D', 'e', 'r', 'e', 'k'},
                        { 'B', 'a', 'n', 'a', 's'}};

    cout << "2nd letter in 2nd array " << myName[1][1] << endl;

    myName[0][2] = 'e';

    cout << "New Value " << myName[0][2] << endl;



    // for loops

    for (int i = 1; i <= 10; i++) {

        cout << i << endl;

    }


    // nested for loops

    for (int j = 0; j < 2; j++) {

        for (int k = 0; k < 5; k++) {

            cout << myName[j][k];

        }

        cout << endl;

    }

    return 0;

}



//// while loop

int main() {

    int randNum = (rand() % 100) + 1;

    while (randNum != 100) {

        cout << randNum << ", ";

        randNum = (rand() % 100) + 1;
    }

    cout << endl;

    return 0;

}



//// Do while loops

int main() {

    string numberGuessed;
    int intNumberGuessed = 0;

    do {

        cout << "Guess the number between 1 and 10: ";

        getline(cin, numberGuessed);

        intNumberGuessed = stoi(numberGuessed);

        cout << intNumberGuessed << endl;

    } while (intNumberGuessed !=4);

    cout << "You win" << endl;

    return 0;

}





//// Strings


int main() {

    char happyArray[6] = {'H','a','p','p','y','\0'};

    string birthdayString = " Birthday";

    cout << happyArray + birthdayString << endl;        // concatenation

    string yourName;
    cout << "What is your name ";
    getline (cin, yourName);                            // take keyboard input
    cout << "Hello " << yourName << endl;

    double eulersConstant = .57721;
    string eulerGuess;
    double eulerGuessDouble;


    cout << "What is Euler's Constant? ";

    getline (cin, eulerGuess);

    eulerGuessDouble = stod(eulerGuess);

    if (eulerGuessDouble == eulersConstant) {

        cout << "You are right" << endl;

    } else {

        cout << "You are wrong" << endl;
    }


    cout << "Size of String " << eulerGuess.size() << endl;     // measure size
    cout << "Is string empty " << eulerGuess.empty() << endl;   // is empty?
    cout << eulerGuess.append(" was your guess") << endl;       // append

    string dogString = "dog";
    string catString = "cat";


    // compare
    cout << dogString.compare(catString) << endl;
    cout << dogString.compare(dogString) << endl;
    cout << catString.compare(dogString) << endl;


    // substring

    string wholeName = yourName.assign(yourName);
    cout << wholeName << endl;

]
    string firstName = wholeName.assign(wholeName, 0, 5);
    cout << firstName << endl;

    int lastNameIndex = yourName.find("Banas", 0);
    cout << "Index for last name is " << lastNameIndex << endl;


    yourName.insert(5, " Justin");                  // insert
    cout << yourName << endl;

    yourName.erase(6,7);                            // erase
    cout << yourName << endl;

    yourName.replace(6,5, "Maximus");               // replace
    cout << yourName << endl;

    return 0;

}




//// Vectors

int main() {

    vector <int> lotterNumVect(10);

    int lotteryNumArray[5] = {4, 13, 14, 24, 34};

    lotterNumVect.insert( lotterNumVect.begin(), lotteryNumArray, lotteryNumArray+3);

    lotterNumVect.insert( lotterNumVect.begin()+5, 44);

    cout << lotterNumVect.at(5) << endl;

    lotterNumVect.push_back(64);                                    // get final value
    
    // Rob's example
    vector < vector <int> > simulation_data;                                    
    simulation_data.push_back(vector <int> ());
    simulation_data[0].push_back(1);
    cout << simulation_data[0][0] << endl;
    simulation_data.push_back(vector <int> ());
    simulation_data[1].push_back(2);
    cout << simulation_data[1][0] << endl;

    cout << "Final Value " << lotterNumVect.back() << endl;         // show final value

    // lotterNumVect.pop_back();                                     // remove final value

    cout << "First Value " << lotterNumVect.front() << endl;        // get first value
    cout << "Empty " << lotterNumVect.empty() << endl;              // return 0 if non-empty
    cout << "Size  " << lotterNumVect.size() << endl;               // size

    return 0;

}





//// Functions

int addNumbers (int firstNum, int secondNum = 0) {

    int combinedValue = firstNum + secondNum;
    return combinedValue;
}


int addNumbers (int firstNum, int secondNum, int thirdNum) {

    return firstNum + secondNum + thirdNum;

}


// Recursive Functions
int getFactorial (int number) {

    int sum;
    if (number == 1) sum = 1;
    else sum = getFactorial(number - 1) * number;
    return sum;
}


// Calling Functions
int main() {

    cout << addNumbers(5) << endl;
    cout << addNumbers(5, 1, 6) << endl;
    cout << "The factorial of 3 is " << getFactorial(3) << endl;

    return 0;

}



//// File I/O

int main() {

    string steveQuote = "A day without sunshine is like, you know, night";

    ofstream writer("stevequote.txt");

    if (! writer) {

        cout << "Error opening file" << endl;
        return -1;

    } else {

        writer << steveQuote << endl;
        writer.close();
    }

    ofstream writer2("stevequote.txt", ios::app);       // append

    if (! writer2) {

        cout << "Error opening file" << endl;
        return -1;

    } else {

        writer2 << "\n -Steve Martin" << endl;
        writer2.close();
    }


    char letter;

    ifstream reader("stevequote.txt");

    if(! reader) {

        cout << "Error opening file" << endl;
        return -1;

    } else {

        for ( int i = 0; !reader.eof(); i++) {      // till end of line

            reader.get(letter);
            cout << letter;

        }

        cout << endl;
        reader.close();

    }

    return 0;

}




//// Exceptions

int main() {

    int number = 0;

    try {

        if (number !=0) {

            cout << 2/number << endl;

        } else throw (number);

    }

    catch (int number) {

        cout << number << " is not valid" << endl;

    }

    return 0;

}




// Monte Carlo Simulation to estimate value of pi

#include <iostream>
#include <math.h>
#include <stdlib.h>
#include <time.h>
 
using namespace std;
int main(){
    int jmax=1000; // maximum value of HIT number. (Length of output file)
    int imax=1000; // maximum value of random numbers for producing HITs.
    double x,y;    // Coordinates
    int hit;       // storage variable of number of HITs
    srand(time(0));
    for (int j=0; j<jmax; j++){
        hit=0;
        x=0; y=0;
        for(int i=0; i<imax; i++){
            x=double(rand())/double(RAND_MAX);
            y=double(rand())/double(RAND_MAX);
        if(y<=sqrt(1-pow(x,2))) hit+=1; }          //Choosing HITs according to analytic formula of circle
    cout<<""<<4*double(hit)/double(imax)<<endl; }  // Print out Pi number
}





