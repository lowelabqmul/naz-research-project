load("age_example_cg22736354.Rdata")
library(marmalaid)
data(annotation_v1.1)
library(RColorBrewer)

age=as.integer(annotation[names(beta_example),]$Age)


plot(age,beta_example,pch=20,col=brewer.pal(n=8,name="Set1")[as.integer(as.factor(annotation[names(beta_example),5]))],xlab="Age (y)",ylab="Methylation")
legend("topleft",levels(as.factor(annotation[names(beta_example),5])),col=brewer.pal(n=8,name="Set1"),pch=20)


load("cancer_example_cg18269493.Rdata")

plot(beta_example,pch=19,cex=0.6,ylim=c(0,1),xaxt="n",xlab="",ylab="Beta",col=brewer.pal(n=8,name="Set1")[as.integer(as.factor(annotation[names(beta_example),8]))])
axis(1,c(mean(which(annotation[names(beta_example),]$DISEASE=="Healthy")),mean(which(annotation[names(beta_example),]$DISEASE=="Cancer"))),c("Healthy","Cancer"))
