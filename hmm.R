
#### Hidden Markov Model (own R method) #####

# vector of state and transition probabilities
# p(1 remain)	 p(1 -> 0)	  p(0 remain)  	 p(0 -> 1)
probability_vector <- c(0.05, 0.95, 0.95, 0.05,
						0.01, 0.99, 0.99, 0.01,
						0.1, 0.9, 0.9, 0.1,	
						0.12, 0.88, 0.9, 0.1,
						0.4, 0.6, 0.7, 0.3,
						0.1, 0.9, 0.9, 0.1,
						0.8, 0.2, 0.75, 0.25,
						0.8, 0.2, 0.75, 0.25,
						0.1, 0.9, 0.88, 0.12,
						0.9, 0.1, 0.7, 0.3,
						0.9, 0.1, 0.8, 0.2)

# convert to matrix and annotate
p_matrix <- matrix(probability_vector, ncol=4, nrow=11, byrow=TRUE)
colnames(p_matrix) <- c("p(1 remain)","p(1 -> 0)", "p(0 remain)","p(0 -> 1)")
rownames(p_matrix) <- paste("CpG ", seq(1,11,1))

# hmm(p_matrix, 1000)

### HMM function

#hmm <- function(p_matrix, simulations) {
	
#	simulation_output <- NULL
	pattern_10 <- matrix(NA, ncol=2)

	# check probabilities in p_matrix
	for(i in 1:nrow(p_matrix)) {
		if(p_matrix[i,1]+p_matrix[i,2] != 1 | p_matrix[i,3]+p_matrix[i,4] != 1) {
			print(paste("Error with probabilities for CpG", i))
			print(p_matrix[i,])
		}
	}

	pattern_10 <- NULL
	
	# Set up Simulations
	simulation <- matrix(0, nrow=100, ncol=11)

	# number of simulation runs
	for(z in 1:10) {

		# simulate for 100 cells
		for(y in 1:nrow(simulation)) {

			# number of CpGs
			for(x in 1:nrow(p_matrix)) {

				# random number generator
				rn <- round(runif(1, 0.0, 1.0), digits=1)

				# Transition 0 -> 1
				if(simulation[y,x] == 0) {if(rn < p_matrix[x,4] | rn == p_matrix[x,4]) { simulation[y,x] <- 1}}

				# Transition 1 -> 0
				else if (simulation[y,x] == 1) {if(rn < p_matrix[x,2] | rn == p_matrix[x,2]) { simulation[y,x] <- 0}}
			}
		}
		
		## catch pattern 10
		if (length(which(simulation[,10]==1 & rowSums(simulation)==1))==0) {pattern_10 <- c(0,pattern_10)}
		else {pattern_10 <- c(length(which(simulation[,10]==1 & rowSums(simulation)==1)), pattern_10)}

	}

		print(simulation)
		print(rev(pattern_10))
		plot(rev(x=pattern_10))


which(simulation[,10]==1 & rowSums(simulation)==1)	
simulation[(which(simulation[,10]==1 & rowSums(simulation)==1)),]


pattern_10 <- c(nrow(simulation[which(simulation[,10]==1 & rowSums(simulation)==1),]), pattern_10)


## Call HMM function
hmm(p_matrix, 100)


















