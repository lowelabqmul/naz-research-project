
###### Plot object and source code ######
# I have added the Robjects and my code in order to make it easier to understand
# where it came from and how it was made
# dont forget to change the user path


#### 1) Number of methylated sites ####

## Robject

load(file = "PATH/binaries.Rdata")

### my code ###

binary_list <- list(ep1_binaries, ep2_binaries, ep3_binaries, 
                    ds4_binaries, ds5_binaries, ds6_binaries)

par(mfrow=c(2,3))

for (y in 1:length(binary_list)) {

	Z_freq <- NULL

	for (x in 1:length(binary_list[[y]])) {

	Z_freq <- rbind(Z_freq, length(grep("1", unlist(strsplit(binary_list[[y]][x], "_")))))

	}

	# barplots for the three EP's and DS's
	barplot(table(Z_freq), ylim=c(0,2000), xlab="Number of methylated sites", ylab="frequency", main="")

}



#### 2) Frequency of patterns containing single 1 across all EP's ####

## Robject

load(file = "PATH/ep_1_pattern.Rdata")

### my code ###

load(file = "PATH/dataframes.Rdata")

library(ggplot2)
library(reshape2)

## column for count of 1's in pattern
ep1$total <- sapply(ep1$Z_binary, function(pattern) {
    sum(as.integer(unlist(strsplit(as.character(pattern), "_"))))
})

ep2$total <- sapply(ep2$Z_binary, function(pattern) {
    sum(as.integer(unlist(strsplit(as.character(pattern), "_"))))
})

ep3$total <- sapply(ep3$Z_binary, function(pattern) {
    sum(as.integer(unlist(strsplit(as.character(pattern), "_"))))
})


## Subset ep1 for Single 1 patterns
ep1_1 <- NULL
ep2_1 <- NULL
ep3_1 <- NULL

ep1_1 <- ep1[which(ep1$total==1),]
ep2_1 <- ep2[which(ep2$total==1),]
ep3_1 <- ep3[which(ep3$total==1),]

row.names(ep1_1)=ep1_1[,1]
row.names(ep2_1)=ep2_1[,1]
row.names(ep3_1)=ep3_1[,1]

row.names(ep1)=ep1[,1]
row.names(ep2)=ep2[,1]
row.names(ep3)=ep3[,1]


## df of common patterns with individual EP frequencies

possible_patterns <- ep2_1$Z_binary

ep_1_pattern <- data.frame(Z_binary = rep(NA,11), ep1=rep(NA,11), ep2 = rep(NA,11), ep3 = rep(NA,11))

rownames(ep_1_pattern) <- possible_patterns

ep_1_pattern$Z_binary = possible_patterns
ep_1_pattern[rownames(ep1_1),2] = ep1_1[,2]
ep_1_pattern[,3] = ep2_1[,2]
ep_1_pattern[rownames(ep3_1),4] = ep3_1[,2]
ep_1_pattern


# reshape dataframe for ggplot2
ep_1_pattern = melt(ep_1_pattern)


## pattern 1 EP ggplot
ggplot(ep_1_pattern,
        aes(x = Z_binary,
            y = value,
            group=variable,
            colour=variable
            )
        ) + geom_point() + stat_summary(fun.data = 'mean_cl_boot',
geom = 'smooth') +
labs(x="Patterns containing a single 1", y="Frequencies") +
theme(axis.text.x = element_text(angle = 30, hjust = 1)) +
ggtitle("Frequency of patterns containing a single 1 across EP's")



#### 3) Frequency of pattern 10 across all passages ####

## Robject
load(file = "PATH/dataframes.Rdata")

### my code ###

## Tracking pattern 0_0_0_0_0_0_0_0_0_1_0

pattern_10 <- NULL
pattern_10 <- rbind(ep1[ep1[,1]=="0_0_0_0_0_0_0_0_0_1_0",], ep2[ep2[,1]=="0_0_0_0_0_0_0_0_0_1_0",], ep3[ep3[,1]=="0_0_0_0_0_0_0_0_0_1_0",]
    , p8_1[p8_1[,1]=="0_0_0_0_0_0_0_0_0_1_0",], p8_2[p8_2[,1]=="0_0_0_0_0_0_0_0_0_1_0",], p8_3[p8_3[,1]=="0_0_0_0_0_0_0_0_0_1_0",]
    , p9_1[p9_1[,1]=="0_0_0_0_0_0_0_0_0_1_0",], p9_2[p9_2[,1]=="0_0_0_0_0_0_0_0_0_1_0",], p9_3[p9_3[,1]=="0_0_0_0_0_0_0_0_0_1_0",]
    , p10_1[p10_1[,1]=="0_0_0_0_0_0_0_0_0_1_0",], p10_2[p10_2[,1]=="0_0_0_0_0_0_0_0_0_1_0",], p10_3[p10_3[,1]=="0_0_0_0_0_0_0_0_0_1_0",]
    , ds4[ds4[,1]=="0_0_0_0_0_0_0_0_0_1_0",], ds5[ds5[,1]=="0_0_0_0_0_0_0_0_0_1_0",], ds6[ds6[,1]=="0_0_0_0_0_0_0_0_0_1_0",])

samples <- c("EP","EP","EP","P8","P8","P8","P9","P9","P9","P10","P10","P10","DS","DS","DS")
samples <- factor(samples, levels = unique(samples) )
row.names(pattern_10) <- NULL

pattern_10 <- cbind(pattern_10, samples)
pattern_10


# plot
ggplot(pattern_10,
        aes(x = samples,
            y = Freq,
            group=Z_binary,
            colour=samples
            )
        ) + geom_point() + stat_summary(fun.data = 'mean_cl_boot',
colour = 'steelblue',
geom = 'smooth') +
labs(x="Pattern '10'", y="Frequencies") + ggtitle("Frequency of pattern '10' across all passages")



#### 4) Average methylation plot ####

## Robject


load(file = "PATH/simulations.Rdata")

### my code ###

## Function to calculate Average Methylation ##

average_methylation <- function(simulation) {

	total <- as.integer(dim(simulation)[1]) * as.integer(dim(simulation)[2])
	count <- 0

	for (x in 1:(dim(simulation)[1])) {
		for (y in 1:(dim(simulation)[2])) {
			if (simulation[x,y] == 0) {
				count <- as.integer(count) + 1
			}
		}
	}

	print(count/total*100)
}


## average methylations for simulations
average_methylation(simulation_1)
average_methylation(simulation_2)
average_methylation(simulation_3)

par(mfrow=c(3,1))
plot(colMeans(simulation_1),typ="l", main = "Average Methylation in Simulation 1", ylab = "CpG methylation means")
plot(colMeans(simulation_2),typ="l", main = "Average Methylation in Simulation 2", ylab = "CpG methylation means")
plot(colMeans(simulation_3),typ="l", main = "Average Methylation in Simulation 3", ylab = "CpG methylation means")





#### 5) Tracking pattern 10 for 100 Simulations ####

## Robject

load(file = "PATH/simulations_10.Rdata")

### my code ###

## Time = rows + 1
simulation_1_pattern_10$V1 <- as.integer(simulation_1_pattern_10$V1) + 1

# column headers
names(simulation_1_pattern_10) <- c("time","frequency")
names(simulation_2_pattern_10) <- c("time","frequency")
names(simulation_3_pattern_10) <- c("time","frequency")

# Set up matrix style dataframe for all simulations
simulations <- matrix(simulation_1_pattern_10$frequency, 11, 100, byrow = FALSE)


row.names(simulations) <- 1:11
simulations <- melt(simulations)

## bind simulation 2 and 3 data
simulations <- cbind(simulations, simulation_2_pattern_10$frequency)
simulations <- cbind(simulations, simulation_3_pattern_10$frequency)

# headers
names(simulations) <- c("CpG","Simulation", "Simulation 1", "Simulation 2", "Simulation 3")

# melt dataframe
simulations <- melt(simulations, id=c("CpG", "Simulation"))

# new headers
names(simulations) <- c("CpG","Runs", "Simulation", "Frequency")


# all simulations in one plot
ggplot(simulations,
        aes(x = CpG,
            y = Frequency,
            group = Runs,
            colour = Simulation
            )
        ) + geom_point(size = 0.9) + 
geom_smooth(aes(x=CpG, y=Frequency, colour = Simulation), level = 0.95, inherit.aes=FALSE) +
labs(x="CpG", y="Frequency") +
ggtitle("All Simulations: Tracking Pattern 10 plot for 100 runs")




