//
//  main.cpp
//  methylation_pattern_simulations
//
//  Created by Naz on 12/04/2016.
//  Copyright © 2016 QMUL. All rights reserved.
//

#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <random>
#include <ctime>

using namespace std;


int main( ) {

    vector < vector <int> > simulation_data(100, vector<int>(11));
    
    // int nCell = 100;       // number of Cells
    // int nCpG  = 11;        // number of CpGs
    
    cout << "        Simulated CpG pattern " << endl;

    
    // Loop to cycle through 100 cells

    for (int j=0; j < 100; j++) {
        
    
        // Loop for the number of time points
        
        for (int k=1; k < 100; k++) {
        
            //// pick a random number between 1 and 11
            
            // Type of random number distribution
            uniform_real_distribution<double> dist(1, 11);  //(min, max)
        
            // Mersenne Twister: Good quality random number generator
            mt19937 rng;
        
            // Initialize with non-deterministic seeds
            rng.seed(random_device{}());
        
            int s;
            s = dist(rng);
        
        
            //// generate 11 random numbers between 0 and 1
            
            // Type of random number distribution
            uniform_real_distribution<double> ran(0, 1);  //(min, max)
    
            // Initialize with non-deterministic seeds
            rng.seed(random_device{}());
        
            
            double r;
            r = ran(rng);
        
        
            // if r is smaller above 50% -> methylation
        
                if (r <= 0.5) {
            
                    simulation_data[j][s] = 0;
            
                } else if (r >= 0.5) {
            
                    simulation_data[j][s] = 1;
            
                }
            
            }
        
        }
    
    
    
    // printing out simulation data
    
    for (int i=0; i<100; i++) {
    
        cout << "cell " << i << ": ";
        
        for (int j=0; j<11; j++) {
        
            cout << simulation_data[i][j];
        
        }
        
        cout << endl;
    }
    
                
    cout << endl;

    return 0;
}
