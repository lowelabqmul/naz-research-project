//
//  simulation_1.cpp
//  methylation_pattern_simulations
//
//  Created by Nazrath 10R on 12/04/2016.
//  Copyright © 2016 QMUL. All rights reserved.
//


#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <random>
#include <ctime>

using namespace std;



//// Function to output Pattern 10 frequency at every time point

void output_function(vector< vector <int> > &in, int nCells, int nCpG,int t) {

    ofstream writer("/Users/nazrathnawaz/Google Drive/Research_Project/simulation_1_pattern_10.txt", ofstream::app);

    int count=0;

    for (int i=0; i<nCells; i++) {

        bool test = true;

        for (int k=0; k<nCpG; k++) {

            if (in[i][k]==1 && k!=9) {

                test = false;
            }

            else if (in[i][k]==0 && k==9) {

                test = false;
            }

        }

        if (test){count+=1;}
    }

    writer << t << "\t" << count << endl;
    writer.close();
}




int main( ) {

//    ofstream writer1("/Users/nazrathnawaz/Google Drive/Research_Project/simulation_1_pattern_10.txt");
//    writer1.close();

    // Define Number of CpG's, Cells and Time points

    int nCells = 1000;       // number of Cells
    int nCpG   = 11;         // number of CpGs
    int nTime  = 11;         // number of Time points
    int nSim   = 100;        // number of repeat Simulation


    for (int n=0; n < nSim; n++) {

        // Set up empty Simulation
        vector < vector <int> > simulation_data(nCells, vector<int>(nCpG));

        cout << "        Simulated CpG pattern " << endl;


        //// Probabilty weightings

        double ep[11] = {1.030576,1,1.120717,1.107855,1.505147,1.036053,1.951521,2.004233,1.0624,3.200193,2.554728};

        // double ds[11] = {1.185845,1.26017,1.277439,1.717013,3.739606,2.102522,45.77679,12.48965,1.771903,14.08516,55.12903};



        ////// Simulation //////


        //// Loop to cycle through Time points

        for (int t=0; t < nTime; t++) {

            //// Loop to cycle through nCells

            for (int j=0; j < nCells; j++) {

                //// CpG

                // Type of random number distribution
                uniform_real_distribution<double> ran(1, nCpG);  //(min, max)

                // Mersenne Twister: Good quality random number generator
                mt19937 rng;

                // Initialize with non-deterministic seeds
                rng.seed(random_device{}());

                // random CpG position
                int k = ran(rng);


                //// Methylation Probabilities

                // Type of random number distribution
                uniform_real_distribution<double> dist(1, 100);  //(min, max)

                // Mersenne Twister: Good quality random number generator
                mt19937 rn;

                // Initialize with non-deterministic seeds
                rn.seed(random_device{}());

                int r;
                r = ep[k] * dist(rn);     // probabilty weighting * random number generated


                // Methylate or not methylate based on probability

                if (r <= 100) {

                    // simulation_data[j][k] = 0;

                } else if (r >= 100) {

                    simulation_data[j][k] = 1;

                }

            }


            // Call output function to store specific pattern for all time points

            output_function(simulation_data,nCells,nCpG,t);


        }

        //  printing out simulation data

        for (int i=0; i<nCells; i++) {

            cout << "cell " << i << ": ";

            for (int j=0; j<nCpG; j++) {

                cout << simulation_data[i][j];

            }

            cout << endl;
        }



        //// Write Simulation to .txt file output

        ofstream writer("/Users/nazrathnawaz/Google Drive/Research_Project/simulation_1.txt", ofstream::app);

        if (! writer) {

            cout << "Error opening file" << endl;
            return -1;

        } else {

            for (int i=0; i<nCells; i++) {

                for (int j=0; j<10; j++) {

                    writer << simulation_data[i][j] << "\t";

                }

                writer << simulation_data[i][10] << endl;
            }

            writer.close();
        }


    }

    return 0;
}
