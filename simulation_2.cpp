//
//  simulation_2.cpp
//  methylation_pattern_simulations
//
//  Created by Nazrath 10R on 12/04/2016.
//  Copyright © 2016 QMUL. All rights reserved.
//


#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <random>
#include <ctime>
#include <iterator>

using namespace std;



//// Function to output Pattern 10 frequency at every time point

void output_function(vector< vector <int> > &in, int nCells, int nCpG,int t){

    ofstream writer("/Users/nazrathnawaz/Google Drive/Research_Project/simulation_2_pattern_10.txt", ofstream::app);

    int count=0;

    for (int i=0; i<nCells; i++) {

        bool test = true;

        for (int k=0; k<nCpG; k++) {

            if (in[i][k]==1 && k!=9) {

                test = false;
            }

            else if (in[i][k]==0 && k==9) {

                test=false;
            }

        }

        if (test){count+=1;}
    }

    writer << t << "\t" << count << endl;
    writer.close();
}




int main( ) {

    ofstream writer1("/Users/nazrathnawaz/Google Drive/Research_Project/simulation_2_pattern_10.txt");
    writer1.close();

    // Define Number of CpG's, Cells and Time points

    int nCells = 1000;       // number of Cells
    int nCpG   = 11;         // number of CpGs
    int nTime  = 11;         // number of Time points
    int nSim   = 100;        // number of repeat Simulation


    for (int n=0; n < nSim; n++) {


        // Set up empty Simulation
        vector < vector <int> > simulation_data(nCells, vector<int>(nCpG));


        cout << "        Simulated CpG pattern " << endl;


        //// Probabilty weightings

        double ep[11] = {0.01006208,0,0.03653046,0.03301726,0.1138209,0.01180162,0.1653592,0.1699297,0.0199195,0.2331673,0.206392};


        //// Set up random numbers ////

        // Type of random number distribution
        uniform_real_distribution<double> ran(0, 1);  //(min, max)

        // Mersenne Twister: Good quality random number generator
        mt19937 rn;

        // Initialize with non-deterministic seeds
        rn.seed(random_device{}());

        // Type of random number distribution
        uniform_real_distribution<double> dist(0, 1);  //(min, max)


        // Mersenne Twister: Good quality random number generator
        mt19937 rng;

        // Initialize with non-deterministic seeds
        rng.seed(random_device{}());



        ////// Simulation //////

        //// Loop to cycle through Time points

        for (int t=0; t < nTime; t++) {


            //// Loop to cycle through nCells

            for (int j=0; j < nCells; j++) {


                //// Methylation Probabilities

                //Should I methylate

                if(ran(rn)<0.5){

                    double r;
                    r = dist(rng);     // probabilty weighting

                    // Methylate or not methylate based on probability

                    if ( 0 < r && r<= ep[0] ) {

                        simulation_data[j][0] = 1;

                    } else if ( ep[0] < r && r <= (ep[0]+ep[1])) {

                        simulation_data[j][1] = 1;

                    } else if ((ep[0]+ep[1]) < r && r <= (ep[0]+ep[1]+ep[2])) {

                        simulation_data[j][2] = 1;

                    } else if ((ep[0]+ep[1]+ep[2]) < r && r <= (ep[0]+ep[1]+ep[2]+ep[3])) {

                        simulation_data[j][3] = 1;

                    } else if ((ep[0]+ep[1]+ep[2]+ep[3]) < r && r<= (ep[0]+ep[1]+ep[2]+ep[3]+ep[4])) {

                        simulation_data[j][4] = 1;

                    } else if ((ep[0]+ep[1]+ep[2]+ep[3]+ep[4]) < r && r <= (ep[0]+ep[1]+ep[2]+ep[3]+ep[4]+ep[5])) {

                        simulation_data[j][5] = 1;

                    } else if ((ep[0]+ep[1]+ep[2]+ep[3]+ep[4]+ep[5]) < r && r <= (ep[0]+ep[1]+ep[2]+ep[3]+ep[4]+ep[5]+ep[6])) {

                        simulation_data[j][6] = 1;

                    } else if ((ep[0]+ep[1]+ep[2]+ep[3]+ep[4]+ep[5]+ep[6]) < r && r <= (ep[0]+ep[1]+ep[2]+ep[3]+ep[4]+ep[5]+ep[6]+ep[7])) {

                        simulation_data[j][7] = 1;

                    } else if ((ep[0]+ep[1]+ep[2]+ep[3]+ep[4]+ep[5]+ep[6]+ep[7]) < r && r <= (ep[0]+ep[1]+ep[2]+ep[3]+ep[4]+ep[5]+ep[6]+ep[7]+ep[8])) {

                        simulation_data[j][8] = 1;

                    } else if ((ep[0]+ep[1]+ep[2]+ep[3]+ep[4]+ep[5]+ep[6]+ep[7]+ep[8]) < r && r <= (ep[0]+ep[1]+ep[2]+ep[3]+ep[4]+ep[5]+ep[6]+ep[7]+ep[8]+ep[9])) {

                        simulation_data[j][9] = 1;


                    } else if ((ep[0]+ep[1]+ep[2]+ep[3]+ep[4]+ep[5]+ep[6]+ep[7]+ep[8]+ep[9]) < r && r <= (ep[0]+ep[1]+ep[2]+ep[3]+ep[4]+ep[5]+ep[6]+ep[7]+ep[8]+ep[9]+ep[10])) {
                        simulation_data[j][10] = 1;
                    }

                }
            }


            // Call output fucntion to store specific pattern for all time points

            output_function(simulation_data,nCells,nCpG,t);


        }



        // printing out simulation data

        for (int i=0; i<nCells; i++) {

            cout << "cell " << i << ": ";

            for (int j=0; j<nCpG; j++) {

                cout << simulation_data[i][j];

            }

            cout << endl;
        }


        cout << endl;



        //// Write Simulation to .txt file output

        ofstream writer("/Users/nazrathnawaz/Google Drive/Research_Project/simulation_2.txt", ofstream::app);

        if (! writer) {

            cout << "Error opening file" << endl;
            return -1;

        } else {

            for (int i=0; i<nCells; i++) {

                for (int j=0; j<10; j++) {

                    writer << simulation_data[i][j] << "\t";


                }

                writer << simulation_data[i][10] << endl;
            }

            writer.close();
        }

    }

    return 0;

}
