//
//  simulation_3.cpp
//  methylation_pattern_simulations
//
//  Created by Nazrath 10R on 12/04/2016.
//  Copyright © 2016 QMUL. All rights reserved.
//


#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <random>
#include <ctime>

using namespace std;



//// Function to output Pattern 10 frequency at every time point

void output_function(vector< vector <int> > &in, int nCells, int nCpG,int t){

    ofstream writer("/Users/nazrathnawaz/Google Drive/Research_Project/simulation_3_pattern_10.txt", ofstream::app);

    int count=0;

    for (int i=0; i<nCells; i++) {

        bool test = true;

        for (int k=0; k<nCpG; k++) {

            if (in[i][k]==1 && k!=9) {

                test = false;
            }

            else if (in[i][k]==0 && k==9) {

                test=false;
            }

        }

        if (test){count+=1;}
    }

    writer << t << "\t" << count << endl;
    writer.close();
}



int main( ) {

    ofstream writer1("/Users/nazrathnawaz/Google Drive/Research_Project/simulation_3_pattern_10.txt");
    writer1.close();

    // Define Number of CpG's, Cells and Time points

    int nCells = 1000;       // number of Cells
    int nCpG   = 11;         // number of CpGs
    int nTime  = 11;         // number of Time points
    int nSim   = 100;        // number of repeat Simulation


    for (int n=0; n < nSim; n++) {

        // Set up empty Simulation

        vector < vector <int> > simulation_data(nCells, vector<int>(nCpG));


        cout << "        Simulated CpG pattern " << endl;


        //// Probabilty weightings

        double ep[11] = {0.1302424,0.001005733,0.107714,0.09735492,0.335613,0.03479835,0.4875792,0.501056,0.05873479,0.6875189,0.6085688};


        ////// Simulation //////
        // Type of random number distribution
        uniform_real_distribution<double> ran(1, nCpG);  //(min, max)

        // Mersenne Twister: Good quality random number generator
        mt19937 rng;

        // Initialize with non-deterministic seeds
        rng.seed(random_device{}());

        // Type of random number distribution
        uniform_real_distribution<double> dist(0, 1);  //(min, max)

        // Mersenne Twister: Good quality random number generator
        mt19937 rn;

        // Initialize with non-deterministic seeds
        rn.seed(random_device{}());


        //// Loop to cycle through Time points

        for (int t=0; t < nTime; t++) {


            //// Loop to cycle through nCells

            for (int j=0; j < nCells; j++) {

                //// CpG


                // random CpG position
                int k = ran(rng);


                //// Methylation Probabilities


                double r;
                r = dist(rn);     // probabilty weighting


                // Methylate or not methylate based on probability

                if (r <= ep[k]) {

                    simulation_data[j][k] = 1;

                } else if (r >= ep[k]) {

                    // simulation_data[j][k] = 0;

                }

            }


            // Call output fucntion to store specific pattern for all time points

            output_function(simulation_data,nCells,nCpG,t);

        }



        // printing out simulation data

        for (int i=0; i<nCells; i++) {

            cout << "cell " << i << ": ";

            for (int j=0; j<nCpG; j++) {

                cout << simulation_data[i][j];

            }

            cout << endl;
        }


        //// Write Simulation to .txt file output

        ofstream writer("/Users/nazrathnawaz/Google Drive/Research_Project/simulation_3.txt", ofstream::app);

        if (! writer) {

            cout << "Error opening file" << endl;
            return -1;

        } else {

            for (int i=0; i<nCells; i++) {

                for (int j=0; j<10; j++) {

                    writer << simulation_data[i][j] << "\t";


                }

                writer << simulation_data[i][10] << endl;
            }

            writer.close();
        }

    }

    return 0;
}
